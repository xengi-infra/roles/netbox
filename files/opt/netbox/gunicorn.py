import multiprocessing

# The IP address (typically localhost) and port that the Netbox WSGI process should listen on
bind = 'unix:/var/run/netbox/gunicorn.sock'

#user = netbox
#group = netbox
#umask = 0022

# Number of gunicorn workers to spawn. This should typically be 2n+1, where
# n is the number of CPU cores present.
workers = multiprocessing.cpu_count() * 2 + 1

# Number of threads per worker process
threads = 2

# Timeout (in seconds) for a request to complete
timeout = 120

# The maximum number of requests a worker can handle before being respawned
max_requests = 5000
max_requests_jitter = 500

